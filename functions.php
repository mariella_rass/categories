<?php


//db connection
function db($host, $userName, $password, $dbName) {

	$db = mysql_connect($host, $userName, $password);

	if (!$db) {
		exit( "Failed to connect to MySQL: " . mysql_error());
	}

	if(!mysql_select_db($dbName,$db)) {
		exit('Failed to get such db');
	}
}

//get arr tree
function getTree() {

	$sql = "SELECT * FROM cat_categories";
	$result = mysql_query($sql);
	if(!$result) {

		exit("Oops, no cats here!");
		return NULL;
	}

	$catsArr = array();

	if( mysql_num_rows($result) != 0 ) {
		
		//new arr
		for($i = 0; $i < mysql_num_rows($result); $i++) {
			$row = mysql_fetch_array($result,MYSQL_ASSOC);
			
			//key is id for parent category
			if( empty($catsArr[$row['cat_parent_id']]) ) {

				$catsArr[$row['cat_parent_id']] = array();
			}	
			$catsArr[$row['cat_parent_id']][] = $row;
		}

		return $catsArr;
	}
}

//cat tree
function catTree($catsArr, $parentCatId = 0) {
	$item = '';
	//if no parent id => getoutofhere
	if( empty($catsArr[$parentCatId]) ) {
		return;
	}

	$item .= '<ul>';

	for($i = 0; $i < count($catsArr[$parentCatId]); $i++) {

		$item .= '<li><a href="?cat_category_id='.$catsArr[$parentCatId][$i]['cat_id'].'&cat_parent_id='.$parentCatId.'">';
		$item .= $catsArr[$parentCatId][$i]['cat_title']. '</a>';
		$item .= catTree( $catsArr, $catsArr[$parentCatId][$i]['cat_id'] );
		$item .= '</li>';
	}

	$item .= '</ul>';

	return $item;
}

function catParents($catsArr, $parentCatId)
{
	if (empty($catsArr[$parentCatId])) {
		return;
	}

	echo '<select onchange="">';
	for ($i = 0; $i < count($catsArr[$parentCatId]); $i++) {
		echo '<option>' .
			$catsArr[$parentCatId][$i]['cat_title'] .
			'</option>';
	}

	echo '</select>';

}


//cat tree in selector
function catTreeSelector($catsArr, $depth = 0, $parentCatId = 0){
	$item = '';
	for($i = 0; $i < count($catsArr[$parentCatId]); $i++) {

			$item .= '<option>';
			$item .= str_repeat("&nbsp;&nbsp;&nbsp;", $depth);
			$item .= $catsArr[$parentCatId][$i]['cat_title'] .'<br/>'.'</option>';
			$item .= catTreeSelector($catsArr, $depth+1, $catsArr[$parentCatId][$i]['cat_id']);
	}
	return $item;
}

function addCat(){




}



?>
