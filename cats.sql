-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 01, 2016 at 02:08 AM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 5.6.26-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cats`
--

-- --------------------------------------------------------

--
-- Table structure for table `cat_categories`
--

CREATE TABLE `cat_categories` (
  `cat_id` tinyint(3) UNSIGNED NOT NULL,
  `cat_title` varchar(255) NOT NULL,
  `cat_parent_id` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cat_categories`
--

INSERT INTO `cat_categories` (`cat_id`, `cat_title`, `cat_parent_id`) VALUES
(1, 'CAT 1', 0),
(2, 'CAT 2', 0),
(3, 'CAT 3', 0),
(4, 'CAT 4', 0),
(5, 'Cat 1-1', 1),
(6, 'CAT 1-2-1', 5),
(7, 'Cat 1-2', 1),
(8, 'Cat 1-3', 1),
(9, 'Cat 2-1', 2),
(10, 'Cat 2-2', 2),
(11, 'Cat 3-3', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cat_categories`
--
ALTER TABLE `cat_categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cat_categories`
--
ALTER TABLE `cat_categories`
  MODIFY `cat_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
